import org.testng.Assert;
import org.testng.annotations.*;

/**
 * Created by algatu on 31/08/2017.
 */

@Test
public class TestClass1 {

    ClassUnderTest cut = new ClassUnderTest();

    @BeforeMethod(groups = "test1")
    public void beforeMethod() {
        System.out.println("BeforeMethod");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("AfterMethod");
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("BeforeTest");
    }

    @AfterTest
    public void afterTest() {
        System.out.println("AfterTest");
    }

    @DataProvider(name = "NameList")
    public Object [][] returnNames() {
        return new Object[][] {
                {"Alex","Constanta"}, {"Silvia","Giurgiu"}, {"Tudor", "Braila"}, {"Daniela", "Bucuresti"}, {"Claudiu", " Buzau"}, {"Dragos","Barlad"}, {"Alina","Tulcea"}
        };
    }

    @DataProvider(name ="TestNumers")
    public Object[][] getNames(){
            return new Object[][] {
                    {1,2,3}, {7,15,22}
            };
    }

    @Test(groups = {"test1", "test1"},description = "test1 e al meu", priority = -10, dataProvider = "TestNumers")
    public void test1(int a, int b, int r) {
        System.out.println("Test1");
        Assert.assertEquals(r, cut.computeSum(a,b));
    }



    @Test(groups = "test2")
    public void test2() {
        System.out.println("Test2");
        Assert.assertEquals("nume=Alex1",cut.customConcat("nume","Alex","="),"Message");
    }

    @Test(groups = "test2", priority = -101, dataProvider = "NameList")
    public void test3(String p1, String p2) {
        System.out.println(p1 + " is from " + p2);

    }

    @BeforeClass
    public void bfc1() {
        System.out.println("Before class1");
    }

}
